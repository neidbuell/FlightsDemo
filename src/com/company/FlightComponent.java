package com.company;

public class FlightComponent {
    private String airline;
    private int flightID;
    private int SUCCESS_ID;
    private CardType cardType;
    private long cardNumber;

    FlightComponent(String airline, int flightID, int SUCCESS_ID, CardType cardType, long cardNumber) {
        this.airline = airline;
        this.flightID = flightID;
        this.SUCCESS_ID = SUCCESS_ID;
        this.cardType = cardType;
        this.cardNumber = cardNumber;
    }

    public boolean isBookedCartFlight(int SUCCESS_ID) {
        return this.SUCCESS_ID == SUCCESS_ID;
    }

    public boolean isCardNumber(long cardNumber) {
        return this.cardNumber == cardNumber;
    }

    public double bookedCardFee() {
        return cardType.getCardFee();
    }

    public CardType bookedCardType() {
        return cardType;
    }

    @Override
    public String toString() {
        return "SID: " + SUCCESS_ID + " airline: " + airline;
    }

}