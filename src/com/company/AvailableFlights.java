package com.company;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class AvailableFlights {
    private List<Flight> flights;

    public AvailableFlights(List<Flight> flights) {
        this.flights= flights;
    }

    public List<CardType> getCardTypes(int flightID) {
        Flight flight = flights.stream()
                .filter( f -> f.isFlight(flightID))
                .findFirst()
                .orElse(new Flight(0, ""));
        return flight.getCardTypes();
    }
}
