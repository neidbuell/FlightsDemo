package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Bookings {
    private List<FlightComponent> flights;

    public Bookings() {
        this(new ArrayList<>());
    }

    public Bookings(List<FlightComponent> flights) {
        this.flights = flights;
    }

    public List getBookedCart(int SUCCESS_ID) {
        return flights.stream()
                .filter( f -> f.isBookedCartFlight(SUCCESS_ID))
                .collect(Collectors.toList());
    }

}
