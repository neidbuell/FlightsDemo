package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Cart {
    List<FlightComponent> bookedFlights;

    public Cart(List<FlightComponent> bookedFlights) {
        this.bookedFlights = bookedFlights;
    }

    public double getCardFees(long cardNumber) {
        return bookedFlights.stream()
                .filter( f -> f.isCardNumber(cardNumber))
                .mapToDouble(FlightComponent::bookedCardFee)
                .sum();
    }

    public String getCardName() {
        return bookedFlights.get(0).bookedCardType().getCardName();
    }

    public CardFeeStats getCardFeeStats() {
        double min = bookedFlights.stream()
                .mapToDouble(FlightComponent::bookedCardFee)
                .min()
                .orElse(0.0);
        double max = bookedFlights.stream()
                .mapToDouble(FlightComponent::bookedCardFee)
                .max()
                .orElse(0.0);
        return new CardFeeStats(min, max, getCardName());
    }

}
