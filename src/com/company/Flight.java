package com.company;

import java.util.ArrayList;
import java.util.List;

public class Flight {
    private int flightID;
    private String airline;
    private List<CardType> cardTypes;

    public Flight(int flightID, String airline, List<CardType> cardTypes) {
        this.flightID = flightID;
        this.airline = airline;
        this.cardTypes = cardTypes;
    }

    public Flight(int flightID, String airline) {
        this.flightID = flightID;
        this.airline = airline;
        this.cardTypes = new ArrayList<>();
    }

    public List<CardType> getCardTypes() {
        return cardTypes;
    }

    public boolean isFlight(int flightID) {
        return this.flightID == flightID;
    }

    @Override
    public String toString() {
        return "FID: " + flightID + " airline: " + airline;
    }
}
