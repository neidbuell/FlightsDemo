package com.company;

public class CardType {
    private String cardName;
    private double cardFee;

    public CardType(String cardName, double cardFee) {
        this.cardName = cardName;
        this.cardFee = cardFee;
    }

    public double getCardFee() {
        return cardFee;
    }

    public String getCardName() {
        return cardName;
    }

    @Override
    public String toString() {
        return "card: " + cardName + " fee: " + cardFee;
    }
}
