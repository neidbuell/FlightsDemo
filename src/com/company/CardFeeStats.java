package com.company;

public class CardFeeStats {
    private double minFee;
    private double maxFee;
    private String cardName;

    public CardFeeStats(double minFee, double maxFee, String cardName) {
        this.minFee = minFee;
        this.maxFee = maxFee;
        this.cardName = cardName;
    }

    public double getMinFee() {
        return minFee;
    }

    public double getMaxFee() {
        return maxFee;
    }

    public String getCardName() {
        return cardName;
    }
}
