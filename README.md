## This is a non-working demo to show a potential solution to the Use Cases and API provided. 

* This was written using TDD. The test cases are part of the documentation. 
* They will show my understanding of the problem set. 
* This is coded to be a minimal solution without guessing what a larger architecture would be. 
* The [TestCart](test/com/company/TestCart.java) class contains the resolved Use Cases.  


![alt text](diagram.png)

The FlightComponent would be the class that was persisted for reporting and tracking.
 

| Column        | Value         |
| ------------- |:-------------:|
| SUCCESS_ID    | 1234          |
| airline       | delta         |
| flightID      | 1201          |
| cardName      | visa          |
| cardFee       | 2.50          |
| cardNumber    | ######        |
| SUCCESS_ID    | 1234          |
| airline       | delta         |
| flightID      | 770           |
| cardName      | visa          |
| cardFee       | 2.25          |
| cardNumber    | ######        |
| SUCCESS_ID    | 555           |
| airline       | frontier      |
| flightID      | 12            |
| cardName      | mastercard    |
| cardFee       | 3.00          |
| cardNumber    | ######        |

rolling up by SUCCESS_ID

One visa and one mastercard transaction. 



