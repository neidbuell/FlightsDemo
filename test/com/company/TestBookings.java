package com.company;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestBookings {

    @Test
    public void BookingsGetBookedCartEmpty() {
        Bookings bookings = new Bookings();
        List<FlightComponent> flights;
        int SUCCESS_ID = 1000;

        flights = bookings.getBookedCart(SUCCESS_ID);
        assertEquals(flights.size(), 0);
    }

    @Test
    public void BookingsGetBookedCarFlightComponent() {
        List<FlightComponent> allFlights = new ArrayList<>();
        List<FlightComponent> targetFlights = new ArrayList<>();

        int targetSUCCESS_ID = 1000;
        String airline = "Delta";
        int flightID = 1201;
        CardType visa = new CardType("visa", 2.00);
        long cardNumber = 1234345645678906L;
        FlightComponent flight = new FlightComponent(airline, flightID, targetSUCCESS_ID, visa, cardNumber);
        allFlights.add(flight);
        targetFlights.add(flight);

        int allSUCCESS_ID = 1001;
        airline = "Delta";
        flight = new FlightComponent(airline, flightID, allSUCCESS_ID, visa, cardNumber);

        allFlights.add(flight);
        Bookings bookings = new Bookings(allFlights);
        List<FlightComponent> results;

        results = bookings.getBookedCart(targetSUCCESS_ID);
        assertEquals(results.size(), 1);
        assertEquals(targetFlights.get(0).toString(),results.get(0).toString());

    }
}
