package com.company;

import org.junit.Test;

import javax.smartcardio.Card;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class TestAvailableFlights {

    @Test
    public void AvailableFlightsGetCardType() {

        List<Flight> flights = new ArrayList<>();

        int deltaFlightID = 1201;
        String deltaAirline = "Delta";
        List<CardType> deltaCardTypes = new ArrayList<>();
        CardType visa = new CardType("visa", 2.00);
        deltaCardTypes.add(visa);
        CardType masterCard = new CardType("visa", 3.00);
        deltaCardTypes.add(masterCard);

        Flight delta = new Flight(deltaFlightID, deltaAirline, deltaCardTypes);
        flights.add(delta);

        int alaskaFlightID = 997;
        String alaskaAirline = "Alaska";
        List<CardType> alaskaCardTypes = new ArrayList<>();
        visa = new CardType("visa", 2.50);
        alaskaCardTypes.add(visa);
        masterCard = new CardType("visa", 4.00);
        alaskaCardTypes.add(masterCard);
        CardType americanExpress = new CardType("american express", 5.00);
        alaskaCardTypes.add(americanExpress);

        Flight alaska = new Flight(alaskaFlightID, alaskaAirline, alaskaCardTypes);
        flights.add(alaska);


        AvailableFlights availableFlights = new AvailableFlights(flights);

        List<CardType> cardResults = availableFlights.getCardTypes(alaskaFlightID);

        assertEquals(cardResults.size(), 3);
    }
}
