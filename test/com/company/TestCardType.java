package com.company;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TestCardType {

    @Test
    public void CardTypeDefault() {
        String cardName = "visa";
        double cardFee = 3.00;
        String identity = "card: " + cardName + " fee: " + cardFee;

        CardType cardType = new CardType(cardName, cardFee);
        assertEquals(cardType.toString(), identity);
    }
}
