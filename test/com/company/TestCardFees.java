package com.company;

import com.company.CardFeeStats;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TestCardFees {

    @Test
    public void CardFeesDefault() {
        double minFee = 2.00;
        double maxFee = 2.25;
        String cardName = "visa";
        CardFeeStats cardFees = new CardFeeStats(minFee, maxFee, cardName);

        assertEquals(cardFees.getMinFee(),minFee);
        assertEquals(cardFees.getMaxFee(),maxFee);
        assertEquals(cardFees.getCardName(),cardName);
    }

}
