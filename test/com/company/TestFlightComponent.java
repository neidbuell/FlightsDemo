package com.company;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TestFlightComponent {

    @Test
    public void FlightComponentDefault() {
        int SUCCESS_ID = 1000;
        String airline = "Delta";
        int flightID = 1201;
        CardType visa = new CardType("visa", 2.00);
        long cardNumber = 1234345645678906L;
        String identity = "SID: " + SUCCESS_ID + " airline: " + airline;
        FlightComponent flightComponent = new FlightComponent(airline, flightID, SUCCESS_ID, visa, cardNumber);
        assertTrue(flightComponent.isBookedCartFlight(SUCCESS_ID));
        assertTrue(flightComponent.isCardNumber(cardNumber));
        assertEquals(identity, flightComponent.toString());
    }
}
