package com.company;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class TestCart {

    @Test
    public void CartTestDefault() {
        List<FlightComponent> flights = new ArrayList<>();

        int SUCCESS_ID = 1000;
        String airline = "Delta";
        int flightID = 1201;
        CardType visa = new CardType("visa", 2.00);
        long cardNumber = 1234345645678906L;
        FlightComponent flight = new FlightComponent(airline, flightID, SUCCESS_ID, visa, cardNumber);
        flights.add(flight);

        flightID = 1201;
        visa = new CardType("visa", 2.25);
        flight = new FlightComponent(airline, flightID, SUCCESS_ID, visa, cardNumber);
        flights.add(flight);

        Bookings bookings = new Bookings(flights);

        // Use Case 1 total fees for all flights
        Cart bookedCart = new Cart(bookings.getBookedCart(SUCCESS_ID));
        double total = bookedCart.getCardFees(cardNumber);
        assertEquals(total, 4.25);

        // Use Case 2 card type used.
        // Assumption that card type in this case was really card name as card type (with different fees) can be different per flight.
        String cardName = bookedCart.getCardName();
        assertEquals(cardName,"visa");

        // Use Case 3 fee ranges min and max;
        CardFeeStats fees = bookedCart.getCardFeeStats();
        assertEquals(fees.getMinFee(),2.00);
        assertEquals(fees.getMaxFee(),2.25);

        // Use Case 4 card type *see Case 2;
        assertEquals(fees.getCardName(),"visa");

        // Use Case 5 the FlightComponent is designed to be persisted in whatever form desired.
        // The SUCCESS_ID and CardType.getCardName() can be used to query.
        // group by SUCCESS_ID to show a single record for a booking.

    }
}
