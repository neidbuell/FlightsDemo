package com.company;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class TestFlight {
    int flightID = 1201;
    String airline = "Delta";
    String identity = "FID: " + flightID + " airline: " + airline;

    @Test
    public void FlightDefault() {
        Flight flight = new Flight(flightID, airline);
        assertEquals(flight.toString(),identity);
        assertTrue(flight.isFlight(flightID));
    }

    @Test
    public void FlightGetCardTypes() {
        List<CardType> cardTypes = new ArrayList<>();
        CardType visa = new CardType("visa", 2.00);
        cardTypes.add(visa);
        CardType masterCard = new CardType("visa", 3.00);
        cardTypes.add(masterCard);

        Flight flight = new Flight(flightID, airline, cardTypes);

        assertEquals(flight.getCardTypes().size(),cardTypes.size());
    }
}
